fetch ("https://treinamentoajax.herokuapp.com/messages/")
.then(resp => resp.json())
.then(console.log)
.catch(console.log)

const nomeAjax = document.getElementById("nome")
const mensagemAjax = document.getElementById("mensagem")
const lista = document.getElementById("lista")

async function post(){//POST

    let nome = nomeAjax.value
    let msg = mensagemAjax.value

    let fetchBody = {
     "message":{
         "name": nome,
         "message": msg
        }
    }
    let fetchConfig = {
     method: "POST",
     headers: {"Content-Type":"application/JSON"},
     body: JSON.stringify(fetchBody)
    }

    await fetch( "https://treinamentoajax.herokuapp.com/messages" , fetchConfig)

    nomeAjax.value = ""
    mensagemAjax.value = ""
}

function get(){//GET
    lista.innerHTML = ""
    fetch("https://treinamentoajax.herokuapp.com/messages")
    .then(response => response.json())
    .then(response => {
        for (i in response){
            let div = document.createElement("div")
            let h3 = document.createElement("h3")
            let p =  document.createElement("p")
            let btnapagar = document.createElement("button")
            let btneditar = document.createElement("button")
            h3.innerHTML = response[i].name
            p.innerHTML = response[i].message
            btnapagar.innerHTML = "Apagar"
            btneditar.innerHTML = "Editar"
            div.appendChild(h3)
            div.appendChild(p)
            div.id = response[i].id
            div.appendChild(btnapagar)
            div.appendChild(btneditar)
            lista.appendChild(div)

            btnapagar.addEventListener("click", function(){
                fetch("https://treinamentoajax.herokuapp.com/messages" + "/" + div.id, {method:"DELETE"}).then(response => {get()})
            })

            btneditar.addEventListener("click", function(){//EDITAR MENSAGENS
                let nameEdit = document.createElement("textarea")
                let msgEdit = document.createElement("textarea")
                let pressEdit = document.createElement("button")
                div.appendChild(nameEdit)
                div.appendChild(msgEdit)
                div.appendChild(pressEdit)

                pressEdit.addEventListener("click", function(){
                    let fetchBody = {
                        "message":{
                            "name": nameEdit.value,
                            "message": msgEdit.value
                        }
                    }
                    let fetchConfig = {
                        method: "PUT",
                        headers: {"Content-Type":"application/JSON"},
                        body: JSON.stringify(fetchBody)

                    }
                fetch("https://treinamentoajax.herokuapp.com/messages" + "/" + div.id, fetchConfig)
                })
            })

            
        }
    })
}

function getID(){//GET ESPECIFICO
    lista.innerHTML = ""
    fetch("https://treinamentoajax.herokuapp.com/messages")
    .then(response => response.json())
    .then(response => {
        for(i in response){
            if(response[i].id == document.getElementById("obterID").value){
                let div = document.createElement("div")
                let h3 = document.createElement("h3")
                let p =  document.createElement("p")
                let btnapagar = document.createElement("button")
                let btneditar = document.createElement("button")
                h3.innerHTML = response[i].name
                p.innerHTML = response[i].message
                btnapagar.innerHTML = "Apagar"
                div.appendChild(h3)
                div.appendChild(p)
                div.id = response[i].id
                div.appendChild(btnapagar)
                lista.appendChild(div)  
                btnapagar.addEventListener("click", function(){
                fetch("https://treinamentoajax.herokuapp.com/messages" + "/" + div.id, {method:"DELETE"}).then(response => {get()})
                })
            }
        }
    })


}